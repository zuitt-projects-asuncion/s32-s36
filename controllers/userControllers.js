// import bcrypt module
const bcrypt = require("bcrypt");

// import User model
const User = require("../models/User");

// import auth module
const auth = require("../auth");

// Controllers

// User Registration

module.exports.registerUser = (req, res) => {

	console.log(req.body);

	/*
		bcrypt- add a layer of security to your user's password

		What bcrypt does is hash our password into a randomized character version of the original string

		syntax: bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized

	*/

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	// Create a new user document out of our user model

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

// Retrieve All Users

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Login 

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	/*
		1. find by user email
		2. if a user email is found, check the password
		3. if we don't find the user email, send a message
		4. if upon checking the found user's password is the same as our input password, we will generate a "key" to access our app. If not, we will turn him away by sending a message to the client
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){

			return res.send("User does not exist");
		
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			 if(isPasswordCorrect){

			 	return res.send({accessToken: auth.createAccessToken(foundUser)})

			 } else {

			 	return res.send("Password is incorrect")
			 }
		}
	})
	.catch(err => res.send(err));
};
