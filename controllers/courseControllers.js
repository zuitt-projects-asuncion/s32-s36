const Course = require("../models/Course");

// Course Registration

module.exports.addCourse = (req, res) => {

	console.log(req.body);

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price,

		/*enrollees: req.body.enrollees,*/
		userId: req.body.userId,

	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};

// Retrieve All Courses

module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

