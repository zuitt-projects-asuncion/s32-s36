const express = require('express');
const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers");

// Routes

// User Registration
router.post("/", userControllers.registerUser);

// Retrieve All Users
router.get("/", userControllers.getAllUsers);

// Login
router.post("/login", userControllers.loginUser);

module.exports = router;
