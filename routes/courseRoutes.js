const express = require('express');
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

router.post("/", courseControllers.addCourse);

router.get("/", courseControllers.getAllCourses);

module.exports = router;